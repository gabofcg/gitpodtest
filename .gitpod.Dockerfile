FROM gitpod/workspace-full

RUN uname -a
RUN sudo apt-get install yara -y
RUN sudo apt install python3-pip -y
RUN sudo pip3 install -U oletools
RUN sudo apt-get install -y whois
RUN sudo apt-get install iputils-ping -y
RUN sudo setcap cap_net_raw,cap_net_admin+e /usr/bin/ping
